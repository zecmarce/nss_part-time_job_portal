# Part-time job portal


### Tím
Sandra Hamráková a Marcel Žec

### Dôležité linky k súborom
[Link na repozitár s implementáciou](https://gitlab.fel.cvut.cz/zecmarce/nss_part-time_job_portal_implementacia)

[Link na celú dokumentáciu v pdf](https://gitlab.fel.cvut.cz/zecmarce/nss_part-time_job_portal/-/blob/master/nss-dokumentacia.pdf)

[Link na high fidelity prototyp](https://www.figma.com/proto/WzlPsmJHBI5ajNmizU1sHq/NSS-Part-time-job-portal-(high-fidelity)?node-id=1%3A2&scaling=min-zoom)

[Link na low fidelity prototyp](https://www.figma.com/proto/06j9vfqRMoguTbojSeFytv/NSS-Part-time-job-portal-(low-fidelity)?node-id=1%3A2&scaling=min-zoom)


### Popis
Aplikácia bude pracovný portál zameraný na brigády. Aplikáciu má v sebe prvky "gamefikácie", čím motivuje uživateľov.  Uživatelia získavjú levely a rôzne achievementy úspešným absolvovaním brigády.

### Motivácia
Motivácia bola v podobnom zadaní na predmete RSP. 
Myšlienka gamefikácia sa nám veľmi páči a preto je zaujímavé ju implementovať.

## Použitá architektúra 
Backend časť aplikácie bude využívať vrstvenú(layered) architektúru zloženú z vrstiev:
- databázová 
- perzistentná (DAOs)
- doménová (models, DTOs)
- aplikačná/business (services)
- prezentačná (REST API kontroléry)

Frontend čas aplikácie bude implementovaná JS knižnicou React a teda bude využívať architektúru Flux.

## Design patterns
* Adapter (wrapper)
* Builder (responseBuilder)
* ChainOfResponsibility (Interceptor)
* Singleton (Entity Manager)
* DI (SpringBoot @Autowired)
* Observer (flux in React)


## SWOT analýza

|  |  |
| ------ | ------ |
| **Strenghts** | - Prax a skúsenosti s implementáciou |
|               | - Nízké náklady na informačný systém |
|               | - Široká ponuka práce v rôznych oblastiach |
|               | - Motivácia pomocou gamifikácie |
|               | - Používatelia získajú konkurenčnú výhodu na trhu práce |
| **Weaknesses** | - Ponuka len part-time job |
|                |-  Málo atraktívne pre strednú a staršiu vekovú kategóriu  |
|                |- Platforma závislá na spolupráci so zamestnávateľmi |
|  **Opportunities** |- Zvýšenie zamestnanosti v ČR a SR |
|                    |- Mladí ľudia hľadajú prácu čím ďalej viac pomocou internetu |
|                    |- Ponuka práce v zahraničí |
|                    |- V dôsledku ekonomickej krízy zvýšenie záujmu o prácu |
| **Threats** | - Konkurencia iných pracovných portálov |
|             | - V dôsledku ekonomickej krízy ohrozenie zníženia počtu ponúk zo strany zamestnávateľa |

## PEST analýza

|  |  |
| ------ | ------ |
| **Political** | Podľa štatistiky PAS - podnikateľská aliancia Slovenska sa podnikateľské prostredie Českej republiky pohybuje na približne priemernej úrovni Európskej únie. Slovenska republika sa však pohybuje pod priemernou úrovňou. Očakáva sa však zlepšenie podnikateľského prostredia v oboch krajinách ako dôsledok naštartovania ekonomiky a zmierňovania dopadov krízy spôsobenej celosvetovou pandémiou. |
| **Economical** | Na Českom aj Slovenskom trhu práce je nezamestnanosť na historických minimách. Táto skutočnosť je ale ovplyvnená aj postupným sprísnením podmienok evidencie nezamestnaných. Aktuálne však prebieha celosvetová pandémia, s ktorou súvisí veľká ekonomická kríza, ktorá bude mať veľký dopad na všetky svetové ekonomiky. Očakáva sa preto, že sa nezamestnanosť podstatne zvýši.    | 
| **Social** | V spoločnosti sa používanie internetu stalo dennodennou súčasťou života. Stále však zaostáva staršia generácia, ktorá sa ale v istých oblastiach musí prispôsobiť. Všeobecne je schopnosť využívania internetu a práce s ním na dobrej úrovni. Využívanie pracovných portálov je preto spoločensky akceptovaná vec, ktorá takmer úplne nahradila staršie metódy inzercie práce. |
| **Technological** | Webové technológie a predovšetkým JavaScript sa neustále vyvíjajú. Zároveň nie je problém s kompatibilitou starších technologií. Je potrebne priebežné sledovať, aké trendy nastupujú a priebežne sa prispôsobovať modernejším technológiám.  | 

## 5F analýza

|  |  |
| ------ | ------ |
| **Existujúci konkurenti** | Na českom trhu existujú 3 veľké konkurenčné pracovné portály a veľa ďalších menšín. Na slovensku je situácia veľmi podobná, pričom je jeden portál zriadený priamo v spolupráci s úradom práce. Za konkurentov však považujeme aj existujúce “pracovné” skupiny na facebooku.  |
| **Potenciálni konkurenti** | Vzhľadom na blížiacu sa ekonomickú krízu v dôsledku celosvetovej pandémie môžu byť potenciálnym konkurentom rôzne projekty, ktoré by robil štát na zmiernenie ekonomickej krízy. V súčasnosti sú potenciálnymi konkurentmi iba nejaké menšie novovzniknuté pracovné portály. |
| **Dodávatelia** | Našimi dodávateľmi sú zamestnávatelia. Ak nepotrebujú a nehľadajú pracovnú silu, nie sú pre nich pracovné portály potrebné. Vzhľadom na ekonomickú volatilitu trhových ekonomík sa nepredpokladá, že by zamestnávatelia boli plne uspokojený so zamestnancami a mali ich dostatok.  |
| **Kupujúci** | Našimi kupujúcimi sú potenciálni zamestnanci. Veľmi podobne ako s dodávateľmi. Všetko sa odvíja od trhového princípu dopytu a ponuky. Pri zákazníkoch sa ale očakáva ešte väčší dopyt po práci ako zo strany zamestnávateľov. A vzhľadom na technologický vývoj a s ním spojené transformácie alebo zánik pracovných pozícií sa očakáva nárast dopytu po práci.  |
| **Substitúty** | Za substitúty našeho portálu môžeme považovať inzerácie zamestnávateľov na ich vlastných webových stránkach. Netreba zabudnúť na staršie formy pracovnej inzercie, tieto formy ale pomaličky zanikajú a nemajú žiaden dopad na náš portál. |

## Užívatelia systému

- **Užívateľ** - môže vytvoriť profil, zobraziť jeho detail, môže sa prihlásiť na brigádu a má možnosť vidieť všetky brigády, ktoré absolvoval. V profile si môže zobraziť aj ponuky práce na ktoré sa prihlásil a ich stav ( žiadosť zamietnutá, schválená, odoslaná) Za jednotlivú brigádu môže získať achievement, ktorý je potrebný k prihláseniu na niektoré typy brigád.
- **Manažér** - môže vytvoriť príspevok s ponukou práce, schvaľovať záujemcov a  po ukončení brigádnickej činnosti môže ohodnotiť výkon pracovníka.
- **Administrátor** - môže mazať príspevky, užívateľov a vytvárať a mazať manažérov.


## Funkčné požiadavky
**Administrátor**
- **FR01**	Systém umožní administrátorovi pridať nového manažéra
- **FR02**	Systém umožní administrátorovi upravovať manažéra
- **FR03**	Systém umožní administrátorovi odstranit manažéra
- **FR04**	Systém umožní administrátorovi pridať achievement, ktorý sa skladá z:
    - názov
    - ikona
    - popis
- **FR05**	Systém umožní administrátorovi upraviť achievement
- **FR06**	Systém umožní administrátorovi odstrániť achievement
- **FR07**	Systém umožní administrátorovi pridať kategóriu pracovných ponúk
- **FR08**	Systém umožní administrátorovi upraviť kategóriu pracovných ponúk
- **FR09**	Systém umožní administrátorovi odstrániť kategóriu pracovných ponúk
- **FR10**	Systém umožní administrátorovi vymazať:
    - uživateľa
    - manažéra
    - pracovnú ponuku
    - hodnotenie
- **FR11**	Systém umožní administrátorovi pridať achievement užívateľovi (napríklad za certifikát)

**Manažér**
- **FR12**	Systém umožní manažérovi pridať pracovnú ponuku, ktorá sa skladá z:
    -názov
    -popis 
    -odmena
    -dátum výkonu práce alebo viac dátumov (pracovný turnus)
    -požadovaný level účastníka
    -počet bodov skúsenosti, ktorý pracovník získa
    -zoznam požadovaných achievementov
    -zoznam achievementov, ktoré pracovník získa
    -kategória
- **FR13**	Systém umožní manažérovi upraviť existujúcu pracovnú ponuku
- **FR14**	Systém umožní manažérovi odstrániť existujúcu pracovnú ponuku
- **FR15**	Systém umožní manažérovi si vybrať z prihlásených pracovníkov na pracovnú ponuku
- **FR16**	Systém umožní manažérovi odhlásiť prihláseného pracovníka z pracovnej ponuky
- **FR17**	Systém umožní manažérovi hodnotiť pracovníka po skončení práce formou:
    - slovný komentár
    - zníženie počtu získaných skúseností za prácu
    - zrušenie udelenia achievementu

**Užívateľ**
- **FR18**	Systém umožní užívateľovi zaregistrovať sa s týmito údajmi:
    - meno a priezvisko
    - email
    - telefónne číslo
    - dátum narodenia
    - bydlisko
    - heslo
- **FR19**	Systém umožní užívateľovi prihlásiť a odhlásiť sa
- **FR20**	Systém umožní užívateľovi prehliadať pracovné ponuky, ktoré:
    - sú pre neho dostupné
    - nie sú pre neho dostupné (nesplňuje požadovaný level alebo achievement)
- **FR21**	Systém umožní užívateľovi prehliadať si detail pracovnej ponuky
- **FR22**	Systém umožní užívateľovi prihlásiť sa na pracovnú ponuku, ktorá je pre neho 
- **FR23**	Systém umožní užívateľovi ohodnotiť absolvovanú prácu
- **FR24**	Systém umožní užívateľovi odhlásiť sa z pracovnej ponuky, na ktorú sa prihlásil pred tým, než manažer ponuky potvrdil jeho prihlášku
- **FR25**	Systém umožní užívateľovi prehliadať históriu absolvovaných práci
- **FR26**	Systém umožní užívateľovi zobraziť si detail profilu (údaje, level, achievementy)
- **FR27**	Systém umožní užívateľovi upravit si svoj profil.
- **FR28**	Systém umožní užívateľovi odstrániť svoje konto

**Systém**
- **FR29**	Systém automaticky pridáva achievement za X absolvovaných prací v určitej kategórií
- **FR30**	Systém automaticky pridáva achievement za každý piaty level získaný užívateľom
 
## Nefunkčné požiadavky
- **NR1**	Aplikácia bude dobre škálovateľná a bude zvládať veľkú záťaž užívateľov
- **NR2**	Aplikácia bude mať podporu na ďalších 5 rokov
- **NR3**	Databáza by bude spĺňať minimálně 3. normálovú formu
- **NR4**	Databáza bude zabezpečená proti SQL útokom
- **NR5**	Aplikácia využije databázu Postgres
- **NR6**	Užívateľské dáta budú spracované podľa európskej normy uchovávania osobných údajov
- **NR7**	Vzhľad užívateľskej časti bude navrhnutý tak, aby ho bolo možné obmieňať po každom roku. 

## Varianty riešenia
#### 1.  varianta
Aplikácia s administrátorskou rolou, ktorá spravuje obsah. Obsah inzerátov by zamestnávatelia zasielali prostredníctvom Google Form alebo emailom. Hodnotenie pracovníka zamestnávateľ zasiela po ukončení práce opäť prostredníctvom Google Forms alebo emailom. Registrovaný uživatelia po prihlásení na pracovnú ponukú čakajú na odpoveď od zamestnanca prostredníctvom emailu alebo telefónu.

**+** jednoduchšia implementácia

**+** nie je potrebné školiť veľké množstvo ľudí

**-** veľmi malá konkurencieschopnosť

**-** náročná a neefektívna udržbá

**-** náročná rozšíriteľnosť funkcionality
#### 2.  varianta 
Aplikácia s administrátorskou ale aj manažérskou rolou. Manažerom je zamestnávateľ, ktorý pridáva inzeráty. Administrátor spravuje iba manažérov, prípadne uživateľov. Uživateľ sa na brigádu prihlasuje cez aplikáciu.

**+** jednoduchšia údržba

**+** jednoduchšia rozšíriteľnosť funkcionality

**-** riziko prihlásenia nezodpovedných uchádzačov

**-** väčšie množstvo školení (uživateľov v roly manažérov)
#### 3.  varianta <-- vybraná varianta
Aplikácia s administrátorskou ale aj manažérskou rolou. Manažerom je zamestnávateľ, ktorý pridáva inzeráty, hodnotení účastníkov brigády a vyberá uchádzačov cez aplikáciu. Administrátor spravuje iba manažérov, prípadne uživateľov. Uživateľ sa na brigádu prihlasuje cez aplikáciu a po ukončení brigády ju môže ohodnotiť.

**+** jednoduchšia údržba

**+** vysoká konkurencieschopnosť

**-** náročnejšia implementácia

**-** väčšie množstvo školení (uživateľov v roly manažérov)

## Matica zodpovednosti (RACI matrix)

|  | Sandra Hamráková | Marcel Žec |
| ------ | ------ |------ |
Funkčné požiadavky |	A |	R, A
Nefunkčné požiadavky |	R, A |	A
Analýza |	A |	R, A
Low fidelity prototype |	R, A |	A
High fidelity prototype |	A |	R, A
Frontend |	I, C |	R, A
Backend |	R, A |	I, C

## Metriky

| kategória | pôsobenie | metrika | 
| ------ | ------ | ------ |
| quality | frontend | Komponenty, ktoré v sebe urdžuju stav sú v podobe tried. Komponenty bez stavu sú v podobe funkcie. |
| quality | frontend | Štruktúra priečinkov a súborov pre CRUD operácie je následovná: <ul><li>/{ResourceNameFolder}/Index.js - list zdrojov</li><li>/{ResourceNameFolder}/Create.js - vytvorenie zdroja</li><li>/{ResourceNameFolder}/Edit.js - uprava zdroja</li><li>/{ResourceNameFolder}/Detail.js - detail zdroja</li></ul>|
| quality | frontend, backend | Netriviálne metodý a triviálne metódy, ktoré sú dlhšie ako 8 riadkov (mimo riadkov s deklaraciou premennej) sú riadne okomentované pred mergeovaním na develop branch.  |
| quality | frontend, backend | Zmeny, ktoré zasahujú do viac ako 1 servisy sa vykonávajú na osobitnej branchy s názvom *feature_{nazovZmeny}*  |  |


